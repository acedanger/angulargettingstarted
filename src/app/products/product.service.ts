import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import {tap, catchError } from 'rxjs/operators';

import { IProduct } from './product';

@Injectable({
    providedIn: 'root'
})

export class ProductService {
    // tslint:disable-next-line:max-line-length
    private productUrl = 'https://gist.githubusercontent.com/acedanger/dd56c1b65e8dd0fba4ef53c99a3488ad/raw/cec61aa46f098445f07b4ad3d3ea376e75239476/ang-getting-started.json';
    // private productUrl = 'api/products/products.json';

    constructor(private http: HttpClient) {}

    getProducts(): Observable<IProduct[]> {
        // automatically mapped to this return type of IProduct
        // what happens if the server changes the object parameters
        return this.http.get<IProduct[]>(this.productUrl).pipe(
          tap(data => console.log('All: ' + JSON.stringify(data))),
          catchError(this.handleError)
        );
    }

    private handleError(err: HttpErrorResponse) {
        // in a real world app, we may send the error to some remote logging infrastructure
        // instead of just logging it to the console
        let errorMessage = '';
        if (err.error instanceof ErrorEvent) {
            // client-side or network error occurred
            errorMessage = `An error occurred: ${err.error.message}`;
        } else {
            // the backend returned an unsuccessful response code
            // the response body may contain clues as to what went wrong
            errorMessage = `Server returned ${err.status}, error message is: ${err.message}`;
        }
        console.error(errorMessage);
        return throwError(errorMessage);
    }
}
