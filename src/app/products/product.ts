export interface IProduct {
    productId: number;
    productName: string;
    productCode: string;
    releaseDate: string; /* could be a date data type. this app only stores as a string */
    price: number;
    description: string;
    starRating: number;
    imageUrl: string;
}
