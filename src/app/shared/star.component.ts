import { Component, OnChanges, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-star',
    templateUrl: './star.component.html',
    styleUrls: ['./star.component.css']
})

export class StarComponent implements OnChanges {
    // @Input here denotes that this value is passed into this component
    @Input() rating: number;
    starWidth: number;
    @Output() ratingClicked: EventEmitter<string> =
        new EventEmitter<string>();

    ngOnChanges(): void {
        // the 75 here matches the width of the stars; see line 4 of start.component.html
        // the 5 below is the max rating of an item (5 stars)
        this.starWidth = this.rating * 75 / 5;
    }

    onClick(): void {
        // this method name matches the method defined in "product-list.component.ts"
        this.ratingClicked.emit(`The rating ${this.rating} was clicked!`);
    }
}
